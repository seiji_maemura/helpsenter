<?php

/**
 * [区分]定数クラス
 *
 * @category ユーティリティ機能
 */

class Constants
{

	/* 環境変数 */
	const ROOT_DIR = 'FormTool';
	const ERR_MSG = '障害が発生しました。担当者まで連絡してください。';

    /* 入力編集項目 */
	const NAME_TEXT = "テキストフィールド";
	const NAME_TEXTAREA = "テキストエリア";
	const NAME_RADIO = "ラジオボタン";
	const NAME_CKECKBOX = "チェックボックス";
	const NAME_SELECT = "プルダウン";
	const NAME_NAME = "名前";
	const NAME_NAME_INPUT1 = "姓";
	const NAME_NAME_INPUT2 = "名";
	const NAME_MAIL = "メールアドレス";
	const NAME_TEL= "電話番号";
	const NAME_AGE = "生年月日";
	const NAME_GENDER = "性別";
	const NAME_JOB = "職業";
	const NAME_ADDER = "住所";
	const NAME_PREFECTURES = "都道府県";
	const NAME_DATE = "日付";
	const NAME_TIME = "時間";
	const NAME_INPUTS = "複数項目";
	const NAME_TEXT_ST = "テキスト";
	const NAME_TITLE_ST = "見出し";

	const INPUT_25_PER = "input25per";
	const INPUT_80_PER = "input80per";
	const INPUT_50_PER = "input50per";

    const OPTIONS = "選択肢1\n選択肢2\n選択肢3";
    const OPTIONS_GENDER= "男性\n女性";
    const OPTIONS_JOB= "中学生\n高校生\n専門学校生\n短大生\n大学生\n大学院生\nその他学生\n専業主婦\n家事手伝い\n会社員\n公務員\n自営業\nフリーター・アルバイト\nアーティスト・クリエイター\nその他";
    const OPTIONS_PREFECTURES = array('北海道','青森県','岩手県','宮城県','秋田県','山形県','福島県','茨城県','栃木県','群馬県','埼玉県','千葉県','東京都','神奈川県','新潟県','富山県','石川県','福井県','山梨県','長野県','岐阜県','静岡県','愛知県','三重県','滋賀県','京都府','大阪府','兵庫県','奈良県','和歌山県','鳥取県','島根県','岡山県','広島県','山口県','徳島県','香川県','愛媛県','高知県','福岡県','佐賀県','長崎県','熊本県','大分県','宮崎県','鹿児島県','沖縄県');

    const DEF_VALUE = "テキストを入力してください";
    const DEF_TITLE_VALUE = "見出しを入力してください";


    /* 編集・完了画面 */
    const BUTTON_CONFIRM = "確認画面へ";
    const BUTTON_SEND = "送信する";
    const BUTTON_RETURN = "前に戻る";
    const BUTTON_CLOSE = "閉じる";
    const CONFIRM_MESSAGE = "上記の入力内容を確認して「確認画面へ」ボタンを押してください";
    const CHECK_MESSAGE = "上記内容でよろしければ「送信する」ボタンを押してください";
    const DONE_MESSAGE = "お問合わせを送信いたしました。ありがとうございました。\n内容を確認後、早急にご返信させていただきます。\nもし数日中に返事が無い場合は、正しく受信できなかった可能性がありますので、恐れ入りますが再度のご連絡をお願いします。";

    /* ファイルディレクトリ */
    const TEMP_FILE_DIR = "TempFileDir";
    const DATA_FILE_DIR = "data";
    const RECEIPT_NUMBER_FILE = "receipt_number_file.txt";

    /* バリデーションデフォルトメッセージ */
    const VALMSG_REQUIRED = "必須項目です";
    const VALMSG_NUMBER = "数値を半角で入力してください";
    const VALMSG_ONLYLETTERSP = "半角アルファベットで入力してください";
    const VALMSG_ONLYLETTERNUMBER = "半角英数で入力してください";
    const VALMSG_KATAKANA = "カタカナのみで入力して下さい";
    const VALMSG_HIRAGANA = "ひらがなのみで入力して下さい";
    const VALMSG_KANA = "ひらがな･カタカナのみで入力して下さい";
    const VALMSG_EMAIL = "メールアドレスが正しくありません";
    const VALMSG_URL = "URLが正しくありません";
    const VALMSG_EQUALS = "入力された値が一致しません";
    const VALMSG_OR = "のどちらかを入力してください";
    const VALMSG_MINSIZE= "文字以上にしてください";
    const VALMSG_MAXSIZE= "文字以下にしてください";
    const VALMSG_TOO_LONG = "入力されたテキストが長すぎます。";

    /* ダウンロード */
    const ZIP_FILENAME = "FormTool";
    const ZIP_EXTENSION= ".zip";
    const ZIP_DIRNAME = "zip";

    /* 環境設定 */
    const MAX_TEXT_LENGTH = 100000;
    const MAX_CSV_SIZE = 5242880;
    const INPUT_DEVICE = "入力者閲覧環境";
    const INPUT_COUNTER = "受付番号";
    const INPUT_COUNTER_INIT = "000000001";

    /* クラス名 */
    const CLASS_INPUT_TYPE02= 'input_type02';
}

?>