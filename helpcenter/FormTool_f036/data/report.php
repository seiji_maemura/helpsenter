<?php

/**
 * [区分]問い合わせデータ確認画面
 *
 * 問い合わせデータ(CSV）のデータ表示を行う
 *
 * @category サブ機能
 */

if (isset($_GET['mode'])) {
	require_once '../parts/util/Constants.php';
	$mode = $_GET['mode'];
} else {
	require_once 'Constants.php';
	$mode = "out";
}


//CSVデータ取得
$csvdata = array();
foreach(glob(Constants::DATA_FILE_DIR.DIRECTORY_SEPARATOR.'*.csv') as $file){
    if(is_file($file)){
        $fileName = htmlspecialchars($file);
        $csvfile = new SplFileObject($fileName);
        $csvfile->setFlags(SplFileObject::READ_CSV);
        foreach ($csvfile as $line) {
            if(!is_null($line[0])){
                $csvdata[] = $line;
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>メールフォーム作成ツール管理画面</title>

		<!-- 共通ライブラリ -->
		<?php if (isset($_GET['mode'])): ?>
		<?php include('../parts/util/CommonLib.php'); ?>
        <link rel="stylesheet" href="/FormTool/common/js/datatables/datatables.min.css"/>
        <script src="/FormTool/common/js/datatables/datatables.min.js"></script>
        <link rel="stylesheet" href="/FormTool/common/js/colorbox/colorbox.css" />
        <script src="/FormTool/common/js/colorbox/jquery.colorbox-min.js"></script>
        <?php else: ?>
        <link href="../css/reset.css" rel="stylesheet" type="text/css">
		<link href="../css/module.css" rel="stylesheet" type="text/css">
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<script src="../js/jquery-1.8.2.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/jquery.exresize.0.1.0.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="datatables.min.css"/>
        <script src="datatables.min.js"></script>
        <link rel="stylesheet" href="colorbox.css" />
        <script src="jquery.colorbox-min.js"></script>
        <link href="module2.css" rel="stylesheet" type="text/css">
        <link href="layout.css" rel="stylesheet" type="text/css">
        <?php endif;?>

        <script>
            $(function() {

                $("#reportTable").DataTable({
                    // 情報表示 無効
                    info: false,
                });

                $(".iframe").colorbox({
                    iframe:true,
                    width:"80%",
                    height:"80%",
                    opacity: 0.7
                });

            });
        </script>

    </head>

    <body>

        <!--wrapper-->
        <div id="wrapper">

            <!--ステータスバー-->
            <div id="status_bar">変更しました。</div>

            <!--ヘッダーー-->
            <header>
                <div id="header">
                    <div class="inner"></div>
                </div>
                <div id="header2">

                </div>
            </header>

            <!--コンテンツ-->
            <div id="content">

                <div class="main">

                    <div class="box_type01">



                        <div class="inner">
                            <div class="free_only heightLine" >


                                <div class="btn_csv" style="display:none;"><a href="#">CSV形式でダウンロードする</a></div>
                                <table id="reportTable" class="table_type_03">
                                    <thead>
                                        <tr class="odd">
                                            <th style="font-weight:bold;">日時</th>
                                            <th style="font-weight:bold;">端末</th>
                                            <th style="font-weight:bold;">送信結果</th>
                                            <th style="font-weight:bold;">入力</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($csvdata as $line): ?>
                                        <tr class="odd">
                                            <td style="width: 130px;"><?php echo($line[1]); ?></td>
                                            <td><?php echo($line[2]); ?></td>
                                            <td style="text-align: center;"><?php if($line[3]) { echo('エラー'); } else { echo('正常'); } ?></td>
                                            <td style="text-align: center;"><a class="iframe" href="reportView.php?id=<?php echo($line[0]); ?>&mode=<?php echo($mode); ?>">詳細</a></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!--/コンテンツ-->
        </div>
        <!--/wrapper-->

        <!--フッター-->
        <footer class="inner"></footer>

    </body>

</html>