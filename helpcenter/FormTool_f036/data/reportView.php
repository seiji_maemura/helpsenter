<?php

/**
 * [区分]問い合わせデータ詳細表示機能
 *
 * 問い合わせデータ一覧画面から「詳細」押下時のアクション
 *
 * @category サブ機能
 */

if ($_GET['mode'] == 'prev') {
    require_once '../parts/util/Constants.php';
} else {
    require_once 'Constants.php';
}

// パラメータ
$id = $_GET['id'];

//CSVデータ取得
$csvDdl ="";
foreach(glob(Constants::DATA_FILE_DIR.DIRECTORY_SEPARATOR.'*.csv') as $file){
    if(is_file($file)){
        $fileName = htmlspecialchars($file);
        $csvfile = new SplFileObject($fileName);
        $csvfile->setFlags(SplFileObject::READ_CSV);
        foreach ($csvfile as $line) {
            if(!is_null($line[0]) && $line[0] == $id){
                $csvDdl = $line[4];
                $csvDdl= str_replace( "\r\n", "<br>", $csvDdl);
                $csvDdl= str_replace( "\n", "<br>", $csvDdl);
                $csvDdl= str_replace( "\r", "<br>", $csvDdl);
                break;
            }
        }
    }
}


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>管理画面</title>


        <!-- 共通ライブラリ -->
        <?php if ($_GET['mode'] == 'prev'): ?>
		<?php include('../parts/util/CommonLib.php'); ?>
        <?php else: ?>
        <link href="../css/reset.css" rel="stylesheet" type="text/css">
        <?php endif;?>

        <style>
            #cboxContent { background: #E7F0F6; }
            html, body { height: 100%; }
            .box-code {
                width: 100%;
                height: 100%;
                margin: 0 auto;
                padding: 20px 20px 0 20px;
                border: none;
                background: #E7F0F6;
                box-sizing: border-box;
            }

            .box-code .font-color-green {
                color: #009933;
            }

            .box-code h3 {
                margin: 0 0 10px 0;
                padding: 0;
                font-size: 107%;
                font-weight: bold;
            }

            .box-code .box-code-body {
                width: 100%;
                margin: 0;
                padding: 20px;
                background: #FFFFFF;
                border: #CCCCCC dashed 1px;
                box-sizing: border-box;
                 min-height: 93%;
                max-height: 93%;
                overflow: auto;
            }

            .font-color-gray {
                color: #666666;
            }


        </style>

    </head>

    <body>
        <div class="box-code clearfix">
            <h3 class="font-color-green">問い合わせデータ</h3>
            <div class="box-code-body">
                <?php echo($csvDdl); ?>
            </div>
        </div>
    </body>

</html>