<?php // 二重投稿禁止対応
	if ('1' == 0) {
		session_start();
		$ticket = md5(uniqid(mt_rand(), true));
		setcookie('cookie_ticket', $ticket);
		$_SESSION['ticket'] = $ticket;
	}?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,user-scalable=no,maximum-scale=1" />
        <title></title>

        <!-- CSS -->
                <link href="css/reset.css" rel="stylesheet" type="text/css">
        <link href="css/module.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">

        <!-- カスタムCSS -->

<style type="text/css" class="custom">
@media screen and ( max-width:768px ) {}.req .box_left:after {display:none;}.or_req .box_left:after {display:none;}.step-wrapper { display: none; }</style>

        <!-- ライブラリ -->
                <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.validate.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/ajaxzip3.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.exresize.0.1.0.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/flatpickr.min.js"></script>
		<script src="js/ja.js"></script>
		<link rel="stylesheet" href="js/flatpickr.min.css">

        <!-- ユーザー指定のリンク  -->


        <!-- トラッキングコード -->

        <!-- TODO どこに書くか検討 -->
        <script type="text/javascript">
            $(function() {

            	var geta = 30;
                var elm = window.parent.document.getElementById("FormTool_f036");
                if (elm) {

                    // 親画面のiframe高さ調整
                    resizeIframe();
                    $("#wrapper").exResize(resizeIframe);
                    function resizeIframe() {

                    	elm.style.height = $("#wrapper").height() + geta + "px";

                        var innerHeight = Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                        );

                        elm.style.height = innerHeight + "px";
                    }
                }


				//バリデーション
                $.extend($.validator.messages, {
                    email: '*メールアドレスが正しくありません',
                    equalTo: '*入力された値が一致しません',
                    minlength: '*{0}文字以上にしてください',
                    maxlength: '*{0}文字以下にしてください',
                    required: '*必須項目です',
                    url: '*URLが正しくありません',
                    number: '*数値を半角で入力してください',
                    hiragana: '*ひらがなのみで入力して下さい',
                    katakana: '*カタカナのみで入力して下さい',
                    alphabet: '*半角アルファベットで入力してください',
                    alphanum: '*半角英数で入力してください	',
                    kana: '*ひらがな･カタカナのみで入力して下さい',
                    or: '*のどちらかを入力してください'
                  });

              	//全角ひらがな･カタカナのみ
                jQuery.validator.addMethod("kana", function(value, element) {
                 return this.optional(element) || /^([ァ-ヶーぁ-ん]+)$/.test(value);
                 },
                );

              	//全角ひらがなのみ
                jQuery.validator.addMethod("hiragana", function(value, element) {
                	return this.optional(element) || /^([ぁ-ん]+)$/.test(value);
                	},
                );

                //全角カタカナのみ
                jQuery.validator.addMethod("katakana", function(value, element) {
                	return this.optional(element) || /^([ァ-ヶー]+)$/.test(value);
                	},
                );

                //半角アルファベット（大文字･小文字）のみ
                jQuery.validator.addMethod("alphabet", function(value, element) {
                	return this.optional(element) || /^([a-zA-z\s]+)$/.test(value);
                	},
                );

                //半角アルファベット（大文字･小文字）もしくは数字のみ
                jQuery.validator.addMethod("alphanum", function(value, element) {
                	return this.optional(element) || /^([a-zA-Z0-9]+)$/.test(value);
                	},
                );

                var group = {
				                }

                var rules = {



				// 入力項目の検証ルールを定義します(後で$.validate({ rules: rules })で設定します)。
				"p002":{},			    }

                $('form').validate({
                	wrapper: 'li',
                	groups: group,
                	rules: rules,
                	//表示位置指定
                    errorPlacement: function(error, element) {
                        var parentId = element.attr('parent');
                        if(parentId) {
							error.insertAfter($('#'+parentId));
                        } else {
                        	error.insertAfter(element);
                        }
                    }
                });


                //ラジオボタン選択による表示切替
                var is_view_chenge = $("#is_view_change").val();
                var changeEventList = JSON.parse(is_view_chenge);
                $('input[type="radio"]').change(changeFunc);
                $(window).load(changeFunc);


                function changeFunc(e) {
                    var is_view_hidden = "";
                    changeEventList.forEach(function(value) {
                        if ($('[name=' + value['ISVIEW'] + ']:checked').val() == value['ISVIEW_TARGET']) {
                            if (value['ISVIEW_DISPLAY'] == "hide") {
                                $('#' + value['TARGET']).parent().parent().parent().fadeOut('fast');
                                is_view_hidden += value['TARGET'] + ",";
                            } else {
                                $('#' + value['TARGET']).parent().parent().parent().fadeIn('fast');
                            }
                        } else {
                            if (value['ISVIEW_DISPLAY'] == "hide") {
                                $('#' + value['TARGET']).parent().parent().parent().fadeIn('fast');
                            } else {
                                $('#' + value['TARGET']).parent().parent().parent().fadeOut('fast');
                                is_view_hidden += value['TARGET'] + ",";
                            }
                        }
                    });
                    $('#is_view_hidden').val(is_view_hidden);
                }
                $(".form_content .confirm_box").hide();
                $(".form_content .input_box .btn").click(function () {
                    if($('#p001-1').is(':checked')) {
                        $(".form_content .input_type02").click();
                         return false;
                    }

                });

				var h1 = window.parent.document.getElementById("title1");
                var title = $(h1).text();
                $('#article').attr("value",title);

                var path = parent.location.href;
                $('#articlePath').attr("value",path);
            });

        </script>

    </head>

    <body>
        <!-- 表示・非表示リスト -->
        <input type="hidden" id="is_view_change" value='[{"ISVIEW":"p001","ISVIEW_TARGET":"いいえ","ISVIEW_DISPLAY":"show","TARGET":"p002"},{"ISVIEW":"p001","ISVIEW_TARGET":"いいえ","ISVIEW_DISPLAY":"show","TARGET":"submit"},{"ISVIEW":"p001","ISVIEW_TARGET":"いいえ","ISVIEW_DISPLAY":"hide","TARGET":"p001"}]'>

        <!--wrapper-->
        <div id="wrapper">

            <!--ステップフロー図-->
            <div class="step-wrapper">
                <div class="step-area">
                    <ul class="clearfix step01-now">
                        <li class="step01"><p><span class="small">STEP1</span><span class="txt">情報の入力</span></p></li>
                        <li class="step02"><p><span class="small">STEP2</span><span class="txt">入力内容の確認</span></p></li>
                        <li class="step03"><p><span class="small">STEP3</span><span class="txt">受付完了</span></p></li>
                    </ul>
                </div>
            </div>

            <!--コンテンツ-->
            <div id="content">
                <div class="main index help-detail ">
                    <div class="form_content feedback">



                        <form action="./index_done.php" method="post" class="form">
                            <input type="hidden" name="fk" value="f036" />
                            <input type="hidden" name="mode" value="out" />
                            <input type="hidden" name="is_view_hidden" id="is_view_hidden" value="">
                            <input type="hidden" name="article" id="article" value="">
                            <input type="hidden" name="articlePath" id="articlePath" value="">
                            <input type="hidden" name="javascript" id="javascript" value="無効">
                            <input type="hidden" name="cookie" id="cookie" value="無効">
                            <input type="hidden" name="flash" id="flash" value="未インストール">
                            <input type="hidden" name="screenSize" id="screenSize" value="">
                            <input type="hidden" name="browserSize" id="browserSize" value="">
                            <input type="hidden" name="screenColor" id="screenColor" value="">


                            <div  class="box tbl_row_2 ">

                                <div class="box_left tit">
                                    この記事は役に立ちましたか？                                </div>
                                <div class="box_right areaBtn">
								<div class="input_box">
                                                                         <div class="input_radio01 clearfix" id="p001"><label><input type="radio" name="p001" value="はい" id="p001-1" class="btn yes"/>&nbsp;はい
</label><label><input type="radio" name="p001" value="いいえ" id="p001-2" parent="" class="btn no"/>&nbsp;いいえ</label></div>                                                                                                            </div>

                                </div>
                            </div>


                            <div  class="box tbl_row_3 ">

                                <div class="box_left">
                                    改善できる点などあればご意見をお寄せください                                </div>
                                <div class="box_right">
								<div class="input_box">
                                                                         <textarea id="p002" name="p002" class="input100per" rows="5" parent="" placeholder="" ></textarea>
                                                                                                            </div>

                                </div>
                            </div>

                                                        <div class="confirm_box mb20 mt10">
                                                            <div>
                                                                <div>
                                                                    <input type="submit" class="input_type02" value="送信" onclick="$('input[name=mode]', '#myForm').val('conf');" id="submit"/>
                                                                </div>
                                                            </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            <!--/コンテンツ-->

        </div>
        <!--/wrapper-->
        <script>
            // 閲覧環境
            document.addEventListener("DOMContentLoaded", function(){
                $(function(){
                    function GetDisplaySize(){
                        this.w = window.screen.width;
                        this.h = window.screen.height;
                        this.bwidth = window.outerWidth;
                        this.bheight = window.outerHeight;

                        return this;
                    }

                    $("input[name='javascript']").val("有効");
                    if(navigator.cookieEnabled){
                        $("input[name='cookie']").val("有効");
                    }

                    var hasFlash = false;

                    try {
                        if (new ActiveXObject('ShockwaveFlash.ShockwaveFlash')) {
                            hasFlash = true;
                        }
                    }
                    catch(e){
                        if (navigator.mimeTypes["application/x-shockwave-flash"] != undefined) {
                            hasFlash = true;
                        }
                    }
                    if(hasFlash) $("input[name='flash']}").val("インストール済み");
                    var obj = GetDisplaySize();

                    $("input[name='screenSize']").val(obj.w + "x" + obj.h);
                    $("input[name='browserSize']").val(obj.bwidth + "x" + obj.bheight);

                    var cbit = screen.colorDepth;
                    if(cbit == 1)
                        $("input[name='screenColor']").val("白黒 (1 bit)");
                    else if(cbit == 4)
                        $("input[name='screenColor']").val("16色 (4 bit)");
                    else if(cbit == 8)
                        $("input[name='screenColor']").val("256色 (8 bit)");
                    else if(cbit == 16)
                        $("input[name='screenColor']").val("65,536色 (16 bit)");
                    else if(cbit == 24)
                        $("input[name='screenColor']").val("1,677万色 (24 bit)");
                    else if(cbit == 32)
                        $("input[name='screenColor']").val("42億色 (32 bit)");
                    else
                        $("input[name='screenColor']").val("不明");

                });
            });
        </script>
    </body>

</html>


