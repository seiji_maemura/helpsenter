<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_POST['articlePath'])){
	$article_path = $_POST['articlePath'];
}
if(isset($_POST['article'])){
	$article_title = $_POST['article'];
}
if(isset($_POST['p001'])){
	$p001t = $_POST['p001'];
}
if(isset($_POST['p002'])){
	$p002t = $_POST['p002'];
}
if(isset($_POST['javascript'])){
	$javascript = $_POST['javascript'];
}
if(isset($_POST['cookie'])){
	$cookie = $_POST['cookie'];
}
if(isset($_POST['flash'])){
	$flash = $_POST['flash'];
}
if(isset($_POST['screenSize'])){
	$screenSize = $_POST['screenSize'];
}
if(isset($_POST['browserSize'])){
	$browserSize = $_POST['browserSize'];
}
if(isset($_POST['screenColor'])){
	$screenColor = $_POST['screenColor'];
}

		$validationStr = array();
		$set_mailchange = '1';
		$set_admin_mail = 'support@officestation.jp';
		$set_div_mail = 'support@officestation.jp';
		$set_to_mail_view = 'オフィスステーション';
		$set_replay_to = 'support@officestation.jp';
		$set_cc = 'ryosuke_otsuki@fandmnet.com';
		$set_bcc = '';
		$set_subject = 'ヘルプセンターからフィードバックがありました';
		$set_to_mail_body = 'ヘルプセンターからフィードバックがありました。

該当URL：{該当URL}
該当記事名：{記事名}

役に立った：{p001}
ご意見：{p002}

【閲覧環境】
取得日時：{取得日時}
OS：{OS}
WEBブラウザ：{WEBブラウザ}
JavaScript：{JavaScript}
Cookie：{Cookie}
Flash Player：{フラッシュ}
画面解像度：{画面解像度}
ブラウザサイズ：{ブラウザサイズ}
モニタ色深度：{モニタ色深度}
IPアドレス：{IPアドレス}
ホスト名：{ホスト名}
User-Agent：{入力者閲覧環境}';
		$parts_str = '[{"form_id":"f036","item_id":"p001","parts_type":"1","parts_id":"3","parts_info":"a:5:{s:4:\"NAME\";s:42:\"\u3053\u306e\u8a18\u4e8b\u306f\u5f79\u306b\u7acb\u3061\u307e\u3057\u305f\u304b\uff1f\";s:4:\"MEMO\";s:0:\"\";s:7:\"OPTIONS\";s:17:\"\u306f\u3044\r\n\u3044\u3044\u3048\";s:8:\"POSITION\";s:1:\"1\";s:9:\"INIT_VIEW\";s:1:\"1\";}","created_at":"2020-04-13 10:34:26","updated_at":"2020-04-13 10:34:26"},{"form_id":"f036","item_id":"p002","parts_type":"1","parts_id":"2","parts_info":"a:7:{s:4:\"NAME\";s:66:\"\u6539\u5584\u3067\u304d\u308b\u70b9\u306a\u3069\u3042\u308c\u3070\u3054\u610f\u898b\u3092\u304a\u5bc4\u305b\u304f\u3060\u3055\u3044\";s:4:\"MEMO\";s:0:\"\";s:4:\"INIT\";s:0:\"\";s:11:\"PLACEHOLDER\";s:0:\"\";s:11:\"INPUT_WIDTH\";s:11:\"input100per\";s:9:\"TEXT_ROWS\";s:1:\"5\";s:9:\"INIT_VIEW\";s:1:\"1\";}","created_at":"2020-04-13 15:26:42","updated_at":"2020-04-13 15:26:42"}]';
		$parts_infos = json_decode($parts_str, true);
		$set_resend_mail = '0';
		$set_resend_from_mail_add = '';
		$set_resend_from_mail_view = '';
		$set_resend_mail_subject = '';
		$set_resend_mail_body = '';
		$compdata = '貴重なご意見をいただきありがとうございます。';
		$is_datasave = '0';
		$resultCode = 0;
		$article2 = '記事名';
		$articleURL = '該当URL';
		$input_date = '取得日時';
		$OS = 'OS';
		$browser2 = 'WEBブラウザ';
		$javascript2 = 'JavaScript';
		$Cookie2 = 'Cookie';
		$FlashPlayer2 = 'フラッシュ';
		$screenSize2 = '画面解像度';
		$browserSize2 = 'ブラウザサイズ';
		$screenColor2 = 'モニタ色深度';
		$ip = 'IPアドレス';
		$host = 'ホスト名';
		$input_device = '入力者閲覧環境';
		$input_counter = '受付番号';
		$receipt_number_file_path = './data/receipt_number_file.txt';
		$input_counter_init = '000000001';
	try {

	//POST判定
	if($_SERVER['REQUEST_METHOD'] === 'POST') {

	// 二重投稿禁止対応
	if ('1' == 0) {
		$expire = time() + '300';
		$value = "true";
		setcookie('cookie_post', $value, $expire);
	}

	//連番ファイル作成
	if(!file_exists($receipt_number_file_path)){
		$receipt_number = $input_counter_init;
	} else {
		$receipt_number_str = file_get_contents($receipt_number_file_path);
		$zero_count = mb_strlen( $receipt_number_str);
		$receipt_number_cnt = intval($receipt_number_str) + 1;
		$receipt_number = str_pad($receipt_number_cnt, $zero_count, 0, STR_PAD_LEFT);
	}
	$receipt_number_file= fopen($receipt_number_file_path, "w");
	fwrite($receipt_number_file, $receipt_number);
	fclose($receipt_number_file);

	//メール送信
	mb_language("ja");
	mb_internal_encoding("UTF-8");

	/** 管理者宛てメール送信 **/

	//宛先
	if ($set_mailchange == 0) {
		$toaddr = $set_admin_mail;
	} else {
		$toaddr = $set_div_mail;
	}

	//送信者の名前
	$fromaddr = "From: " . mb_encode_mimeheader ($set_to_mail_view,"ISO-2022-JP-MS") . "<" . $set_admin_mail . ">";
	//Repay-to
	if ($set_replay_to) {
		$fromaddr .= "\r\n" . "Reply-To: " . $set_replay_to;
	}
	//Cc
	if ($set_cc) {
		$fromaddr .= "\r\n" . "Cc: " . $set_cc;
	}
	//Bcc
	if ($set_bcc) {
		$fromaddr .= "\r\n" . "Bcc: " . $set_bcc;
	}

	//メールの件名
	$subject = $set_subject;

	// 本文の文字列置き換え
	foreach ($parts_infos as $parts_info) {
		$pk = $parts_info['item_id'];
		$pt = $parts_info['parts_type'];
		$pi= $parts_info['parts_id'];
        $parts_ddl= unserialize($parts_info['parts_info']);

		//メール本文
		$relaceItem = "";
		switch ($pt) {
			case 1:
				if (isset($_POST[$pk])) {
					$relaceItem =  $_POST[$pk];
				}
				break;
			case 2:

				switch ($pi) {
					case 1:
						if (isset($_POST[$pk.'-1']) && isset($_POST[$pk.'-2'])) {
							$relaceItem = $parts_ddl['INPUT1_TITLE'] . $_POST[$pk.'-1']. $parts_ddl['INPUT2_TITLE']. $_POST[$pk.'-2'];
						}
						break;
					case 2:
					case 5:
					case 6:
					case 8:
					case 4:
					case 9:
					case 10:
						if (isset($_POST[$pk])) {
							$relaceItem =  $_POST[$pk];
						}
						break;
					case 3:
						if (isset($_POST[$pk.'-1']) && isset($_POST[$pk.'-2']) && isset($_POST[$pk.'-3'])) {
							$relaceItem =  $_POST[$pk.'-1']. '-'.$_POST[$pk.'-2']. '-'.$_POST[$pk.'-3'];
						}
						break;
					case 7:
						if (isset($_POST[$pk.'-1']) && isset($_POST[$pk.'-2']) && isset($_POST[$pk.'-3']) && isset($_POST[$pk.'-4'])) {
							$relaceItem =  $_POST[$pk.'-1'].$_POST[$pk.'-2'].$_POST[$pk.'-3'].$_POST[$pk.'-4'];
							if (isset($_POST[$pk.'-5'])) {
								$relaceItem .= $_POST[$pk.'-5'];
							}
						}
						break;
				}
				break;
		}

		$set_to_mail_body = str_replace ("{" . $pk. "}", $relaceItem, $set_to_mail_body);


        //完了メッセージ置換
		$compdata= str_replace ("{" . $pk. "}", $relaceItem, $compdata);
	}

	//該当URL
	$set_to_mail_body = str_replace ("{" . $articleURL. "}", $article_path, $set_to_mail_body);

	//記事名
	$set_to_mail_body = str_replace ("{" . $article2. "}", $article_title, $set_to_mail_body);

	//日付
	$set_to_mail_body = str_replace ("{" . $input_date. "}", date("Y/m/d-H:i:s"), $set_to_mail_body);

	// OS
	$set_to_mail_body = str_replace ("{" . $OS. "}", PHP_OS, $set_to_mail_body);

	// browser
	$ua = getenv('HTTP_USER_AGENT');
	if (strstr($ua, 'Edge') || strstr($ua, 'Edg')) {
		$ua = "Microsoft Edge";
	} elseif (strstr($ua, 'Trident') || strstr($ua, 'MSIE')) {
		$ua = "Microsoft Internet Explorer";
	} elseif (strstr($ua, 'OPR') || strstr($ua, 'Opera')) {
		$ua = "Opera";
	} elseif (strstr($ua, 'Chrome')) {
		$ua = "Google Chrome";
	} elseif (strstr($ua, 'Firefox')) {
		$ua = "Firefox";
	} elseif (strstr($ua, 'Safari')) {
		$ua = "Safari";
	} else {
		$ua = "Unknown";
	}
	$set_to_mail_body = str_replace ("{" . $browser2. "}", $ua, $set_to_mail_body);

	//javascript
	$set_to_mail_body = str_replace ("{" . $javascript2. "}", $javascript, $set_to_mail_body);

	//Cookie
	$set_to_mail_body = str_replace ("{" . $Cookie2. "}", $cookie, $set_to_mail_body);

	//FlashPlayer
	$set_to_mail_body = str_replace ("{" . $FlashPlayer2. "}", $flash, $set_to_mail_body);

	// screenSize
	$set_to_mail_body = str_replace ("{" . $screenSize2. "}", $screenSize, $set_to_mail_body);

	// browserSize
	$set_to_mail_body = str_replace ("{" . $browserSize2. "}", $browserSize, $set_to_mail_body);

	// screenColor
	$set_to_mail_body = str_replace ("{" . $screenColor2. "}", $screenColor, $set_to_mail_body);

	// ip
	$set_to_mail_body = str_replace ("{" . $ip. "}", $_SERVER["REMOTE_ADDR"], $set_to_mail_body);

	// host
	$set_to_mail_body = str_replace ("{" . $host. "}", gethostbyaddr($_SERVER['REMOTE_ADDR']), $set_to_mail_body);

	//入力者環境
	$set_to_mail_body = str_replace ("{" . $input_device. "}", $_SERVER["HTTP_USER_AGENT"], $set_to_mail_body);

	//受付番号
    $set_to_mail_body = str_replace ("{" . $input_counter. "}", $receipt_number, $set_to_mail_body);

	$mailbody = mb_convert_encoding ($set_to_mail_body, "ISO-2022-JP-MS", "UTF-8");
	if (!mb_send_mail ($toaddr, $subject, $mailbody, $fromaddr)) {
		throw  new Exception();
	}


	/** 返信用メール送信 **/
	if ($set_resend_mail == 1) {
		//宛先
		$toaddr_re = $_POST[$set_resend_from_mail_add];

		//送信者の名前
		$fromaddr_re = "From: " . mb_encode_mimeheader ($set_resend_from_mail_view) . "<" . $toaddr . ">";

		//メールの件名
		$subject_re = $set_resend_mail_subject;

		// 本文の文字列置き換え
		foreach ($parts_infos as $parts_info) {
			$pk = $parts_info['item_id'];
			$pt = $parts_info['parts_type'];
			$pi= $parts_info['parts_id'];
	        $parts_ddl= unserialize($parts_info['parts_info']);

			//メール本文
			$relaceItem = "";
			switch ($pt) {
				case 1:
					if (isset($_POST[$pk])) {
						$relaceItem =  $_POST[$pk];
					}
					break;
				case 2:

					switch ($pi) {
						case 1:
							if (isset($_POST[$pk.'-1']) && isset($_POST[$pk.'-2'])) {
								$relaceItem = $parts_ddl['INPUT1_TITLE'] . $_POST[$pk.'-1']. $parts_ddl['INPUT2_TITLE']. $_POST[$pk.'-2'];
							}
							break;
						case 2:
						case 5:
						case 6:
						case 8:
						case 4:
						case 9:
						case 10:
							if (isset($_POST[$pk])) {
								$relaceItem =  $_POST[$pk];
							}
							break;
						case 3:
							if (isset($_POST[$pk.'-1']) && isset($_POST[$pk.'-2']) && isset($_POST[$pk.'-3'])) {
								$relaceItem =  $_POST[$pk.'-1']. '-'.$_POST[$pk.'-2']. '-'.$_POST[$pk.'-3'];
							}
							break;
						case 7:
							if (isset($_POST[$pk.'-1']) && isset($_POST[$pk.'-2']) && isset($_POST[$pk.'-3']) && isset($_POST[$pk.'-4'])) {
								$relaceItem =  $_POST[$pk.'-1'].$_POST[$pk.'-2'].$_POST[$pk.'-3'].$_POST[$pk.'-4'];
								if (isset($_POST[$pk.'-5'])) {
									$relaceItem .= $_POST[$pk.'-5'];
								}
							}
							break;
					}
					break;
			}

			$set_resend_mail_body = str_replace ("{" . $pk. "}", $relaceItem, $set_resend_mail_body);
		}

		//受付番号
    	$set_resend_mail_body = str_replace ("{" . $input_counter. "}", $receipt_number, $set_resend_mail_body);

		$mailbody_re= mb_convert_encoding ($set_resend_mail_body, "iso-2022-jp", "UTF-8");
		if (!mb_send_mail ($toaddr_re, $subject_re, $mailbody_re, $fromaddr_re)) {
			throw  new Exception();
		}
	}

	//データ保存
	if ($is_datasave == 0) {

	    require_once './data/Constants.php';
		$fk = "f036";

		/**
		 * データ作成
		 */
		$csvDir = Constants::DATA_FILE_DIR.DIRECTORY_SEPARATOR.Constants::DATA_FILE_DIR.DIRECTORY_SEPARATOR;
		$csvfile_path = $csvDir . $fk.".csv";
		$line = array($receipt_number, date("Y/m/d H:i:s"), $article_path, $article_title, $p001t, $p002t, PHP_OS, $ua, $javascript, $cookie, $flash, $screenSize, $browserSize, $screenColor, $_SERVER["REMOTE_ADDR"], gethostbyaddr($_SERVER['REMOTE_ADDR']), $_SERVER["HTTP_USER_AGENT"]);

		/**
		 * ディレクトリ作成処理
		 */
		if(!file_exists($csvDir)){
			if(!mkdir($csvDir)){
				throw new Exception();
			}
		}

		/**
		 * ファイル作成
		 */
		if(file_exists($csvfile_path)){
			//ファイルサイズチェック
			$csvfile_size = filesize($csvfile_path);
			if ($csvfile_size >= Constants::MAX_CSV_SIZE) {
				//リネーム
				$iterator = new GlobIterator(getcwd().DIRECTORY_SEPARATOR.$csvDir."*.csv");
				$fCount = $iterator->count();
				rename($csvfile_path, $csvDir.$fk."_".$fCount.".csv");
			}
		}
		$csvfile = fopen($csvfile_path, "a");
		if ($csvfile) {
			fputcsv($csvfile, $line);
		}
		fclose($csvfile);
	}

	//リロード対策
    header("Location: " . $_SERVER['PHP_SELF'], true, 303);
    exit;
  }

} catch (Exception $e) {
	$validationStr[] = "障害によりメールの送信ができませんでした。<br />ご迷惑をお掛けします。";
    $resultCode= 0;
	$set_to_mail_body = $e;
}?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta name="viewport" content="width=device-width,user-scalable=no,maximum-scale=1" />

        <!-- ライブラリ -->
                <link href="css/reset.css" rel="stylesheet" type="text/css">
        <link href="css/module.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.exresize.0.1.0.js" type="text/javascript" charset="utf-8"></script>

        <!-- ユーザー指定のリンク  -->

        <!-- トラッキングコード -->


        <!-- TODO どこに書くか検討 -->
        <script type="text/javascript">
            $(function() {

                // 親画面のiframe高さ調整
                var geta = 30;
                var elm = window.parent.document.getElementById("FormTool_f036");
                if (elm) {
                    resizeIframe();

                    //表示位置を先頭に
        		    var pHeader = $('header',parent.document).height();
        		    if (pHeader) {
	        		    var offset = $('#FormTool_f036',parent.document).offset();
	        		    $(window.parent).scrollTop(offset.top - pHeader);
        		    }

                    $("#wrapper").exResize(resizeIframe);
                    function resizeIframe() {

                    	elm.style.height = $("#wrapper").height() + geta + "px";

                        var innerHeight = Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                        );

                        elm.style.height = innerHeight + "px";
                    }
				}
            });
        </script>


        <!-- カスタムCSS -->

<style type="text/css" class="custom">
.message-done{font-weight: 700;}@media screen and ( max-width:768px ) { #content { width: 100% !important; } }.box_left, .box_right { padding: 5px; }.req .box_left:after {display:none;}.or_req .box_left:after {display:none;}.step-wrapper { display: none; }</style>

    </head>

    <body id="preview">

        <!--wrapper-->
        <div id="wrapper">

            <!--コンテンツ-->
            <div id="content">
                <div class="main index_done">

                    <!-- エラーメッセージ表示 -->
                    <div class="errMessageBox">
                        <ul>
                            <!-- モードによって表示方法を変更 -->
                                                        <?php foreach ($validationStr as $errMsg): ?>
 	           	<li><?php echo($errMsg); ?></li>
 	           <?php endforeach; ?>
                                                    </ul>
                    </div>

                    <!-- 完了メッセージ -->
                    <!-- モードによって表示方法を変更 -->
                                        <?php if (count($validationStr) == 0): ?>
					<div class="message-done">貴重なご意見をいただきありがとうございます。</div>
                    <?php endif; ?>

                </div>
            </div>
            <!--/コンテンツ-->

        </div>
        <!--/wrapper-->
    </body>

</html>